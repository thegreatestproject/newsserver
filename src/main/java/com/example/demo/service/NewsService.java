package com.example.demo.service;

import com.example.demo.dto.NewsDTO;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NewsService {


    public List<NewsDTO> getNewsListByTag(String tag) {
        return null;
    }

    public List<NewsDTO> getNewsListByAuthor(String authorEmail) {
        return null;
    }


    public List<NewsDTO> getNewsListBySearch(String search) {
        return null;
    }

    public List<NewsDTO> getBookmarks(String email) {
        return null;
    }

    public NewsDTO getNewsById(Integer id) {
        return null;
    }
}
